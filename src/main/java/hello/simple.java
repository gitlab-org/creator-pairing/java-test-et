package hello;

import io.sentry.Sentry;

class Simple {
    public static void main(String args[]){

      Sentry.init(options -> {
        //options.setDsn("http://glet_5feefd2d1d9932f56cd32dafc3b708ba@127.0.0.1:3000/api/v4/error_tracking/collector/2");
        options.setDsn("https://glet_7c066475994e94105d8cf405c860b740@gitlab.com/api/v4/error_tracking/collector/29123357");
      });

      try {
        throw new Exception("This is a JAVA test.");
      } catch (Exception e) {
        Sentry.captureException(e);
      }
    }
}
